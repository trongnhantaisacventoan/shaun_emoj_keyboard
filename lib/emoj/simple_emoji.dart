import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shaun_emoj_keyboard/emoj/simple_bottom_bar.dart';
import 'category_bar.dart';
import 'simple_emoji_page.dart';
import 'util/emoji.dart';
import 'util/storage.dart';

/// The emoji keyboard. This holds all the components of the keyboard.
/// This will include the:
///   - category bar
///     This holds the categories
///   - bottom bar
///     This holds the backspace, search and normal space functionality
///   - emoji pages
///     These hold all the emojis in 9 separate listviews.
class SimpleEmojiKeyboard extends StatefulWidget {
  final TextEditingController emotionController;
  final double emojiKeyboardHeight;
  final bool showEmojiKeyboard;
  final bool darkMode;

  SimpleEmojiKeyboard(
      {Key? key,
      required this.emotionController,
      this.emojiKeyboardHeight = 350,
      this.showEmojiKeyboard = true,
      this.darkMode = false})
      : super(key: key);

  _EmojiBoard createState() => _EmojiBoard();
}

/// The emojiboard has a configurable textfield which is will control
/// It has a configurable height and it can be made visible or invisible
/// using the showKeyboard boolean
/// It also has a darkmode for the users with a good taste in styling.
class _EmojiBoard extends State<SimpleEmojiKeyboard> {
  /// The name of the channel that Android will call when adding an emoji.
  /// This function will see if it can be shown in the Android version
  /// that the user is currently using.
  /// (See MainActivity in the android project for the implementation)
  static const platform =
      const MethodChannel("com.trongnhantaisacventoan.shaun_emoj_keyboard");

  final GlobalKey<CategoryBarState> categoryBarStateKey =
      GlobalKey<CategoryBarState>();
  final GlobalKey<SimpleEmojiPageState> emojiPageStateKey =
      GlobalKey<SimpleEmojiPageState>();

  FocusNode focusSearchEmoji = FocusNode();

  final TextEditingController searchController = TextEditingController();
  List<String> searchedEmojis = [];

  late TextSelection rememberPosition;

  double emojiKeyboardHeight = 350;

  TextEditingController? bromotionController;

  bool showBottomBar = true;
  bool darkMode = false;
  List<Emoji> recent = [];
  List<String> recentEmojis = [];

  final storage = Storage();

  @override
  void initState() {
    this.bromotionController = widget.emotionController;
    this.emojiKeyboardHeight = widget.emojiKeyboardHeight;
    this.darkMode = widget.darkMode;

    storage.fetchAllEmojis().then((emojis) {
      if (emojis.isNotEmpty) {
        recent = emojis;
        recent.sort((a, b) => b.amount.compareTo(a.amount));
        recentEmojis = recent.map((emote) => emote.emoji).toList();

        if (mounted) {
          setState(() {});
        }
      }

      if (recent.isEmpty) {
        categoryHandler(1);
        switchedPage(1);
      } else {
        categoryHandler(0);
        switchedPage(0);
      }
    });

    super.initState();
  }

  @override
  void didUpdateWidget(covariant SimpleEmojiKeyboard oldWidget) {
    this.darkMode = widget.darkMode;
    this.emojiKeyboardHeight = widget.emojiKeyboardHeight;
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    focusSearchEmoji.dispose();
    super.dispose();
  }

  /// This function handles any changes to the category from the
  /// category bar and passes it to the emoji page widget.
  void categoryHandler(int categoryNumber) {
    if (emojiPageStateKey.currentState != null) {
      emojiPageStateKey.currentState!.navigateCategory(categoryNumber);
    }
  }

  /// This function handles changes in the Emoji page if the user swipes
  /// left or right.
  /// It sends a trigger to the category bar to update the category
  void switchedPage(int pageNumber) {
    if (categoryBarStateKey.currentState != null) {
      categoryBarStateKey.currentState!.updateCategoryBar(pageNumber);
    }
  }

  /// If the user presses an emoji it is added to it's "recent" list.
  /// This is a list of emojis in a local db
  /// It looks to see if it is present in the 'recent emoji' list.
  /// If that is true than it should be in the database and we update it.
  /// If that is not true, we add a new entry for the database.
  /// When it adds a new entry it will look in the emoji list for the category
  /// that the emoji is in to be able to store a new entry in the local db
  void addRecentEmoji(String emoji, int category) async {
    List<String> recentEmojiList = recent.map((emote) => emote.emoji).toList();
    if (recentEmojiList.contains(emoji)) {
      // The emoji is already in the list so we want to update it.
      Emoji currentEmoji = recent.firstWhere((emote) => emote.emoji == emoji);
      currentEmoji.increase();
      storage.updateEmoji(currentEmoji).then((value) {
        recent.sort((a, b) => b.amount.compareTo(a.amount));
        setState(() {
          recentEmojis = recent.map((emote) => emote.emoji).toList();
        });
      });
    } else {
      Emoji newEmoji = Emoji(emoji, 1);
      storage.addEmoji(newEmoji).then((emotion) {
        recent.add(newEmoji);
        recent.sort((a, b) => b.amount.compareTo(a.amount));
        setState(() {
          recentEmojis = recent.map((emote) => emote.emoji).toList();
        });
      });
    }
  }

  /// This function is called when we want to see if any of the recent emojis
  /// that the user used can be shown in this Android version.
  isAvailable(recentEmojis) {
    if (Platform.isAndroid) {
      Future.wait([getAvailableEmojis(recentEmojis)]).then((var value) {
        setState(() {});
      });
    } else {
      setState(() {
        this.searchedEmojis = recentEmojis;
      });
    }
  }

  /// If the emoji cannot be shown in this Android version it is removed from
  /// the list.
  Future getAvailableEmojis(emojis) async {
    List availableResult =
        await (platform.invokeMethod("isAvailable", {"emojis": emojis}));
    List<String> availables = [];
    for (var avail in availableResult) {
      availables.add(avail.toString());
    }
    this.searchedEmojis = availables;
  }

  /// If the user selects an emoji from the grid a trigger is send to this
  /// function with the corresponding emoji that the user pressed.
  /// The emoji is added to the Textfield at the location of the cursor
  /// or as a replacement of the selection of the user.
  void insertText(String myText, int category) {
    addRecentEmoji(myText, category);
    final text = bromotionController!.text;
    final textSelection = bromotionController!.selection;
    final newText = text.replaceRange(
      textSelection.start,
      textSelection.end,
      myText,
    );
    final myTextLength = myText.length;
    bromotionController!.text = newText;
    bromotionController!.selection = textSelection.copyWith(
      baseOffset: textSelection.start + myTextLength,
      extentOffset: textSelection.start + myTextLength,
    );
  }

  bool isPortrait() {
    return MediaQuery.of(context).orientation == Orientation.portrait;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: true,
      top: false,
      child: Container(
        height: widget.showEmojiKeyboard
            ? isPortrait()
                ? emojiKeyboardHeight
                : 150
            : 0,
        color: this.darkMode ? Color(0xff373737) : Color(0xffc5c5c5),
        child: Column(
          children: [
            CategoryBar(
                key: categoryBarStateKey,
                categoryHandler: categoryHandler,
                darkMode: darkMode),
            Stack(
              children: [
                SimpleEmojiPage(
                    key: emojiPageStateKey,
                    emojiKeyboardHeight:
                        isPortrait() ? emojiKeyboardHeight : 150,
                    bromotionController: bromotionController!,
                    emojiScrollShowBottomBar: (p0) {},
                    insertText: insertText,
                    recent: recentEmojis,
                    switchedPage: switchedPage),
                SimpleBottomBar(
                    bromotionController: bromotionController!,
                    darkMode: darkMode),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
