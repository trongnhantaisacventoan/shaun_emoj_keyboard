import 'package:flutter/material.dart';

/// The emoji keyboard. This holds all the components of the keyboard.
/// This will include the:
///   - category bar
///     This holds the categories
///   - bottom bar
///     This holds the backspace, search and normal space functionality
///   - emoji pages
///     These hold all the emojis in 9 separate listviews.
class SimpleEmojiBar extends StatefulWidget {
  final TextEditingController emotionController;
  final double emojiKeyboardHeight;
  final bool showEmojiKeyboard;
  final bool darkMode;

  SimpleEmojiBar(
      {Key? key,
      required this.emotionController,
      this.emojiKeyboardHeight = 350,
      this.showEmojiKeyboard = true,
      this.darkMode = false})
      : super(key: key);

  _EmojiBoard createState() => _EmojiBoard();
}

/// The emojiboard has a configurable textfield which is will control
/// It has a configurable height and it can be made visible or invisible
/// using the showKeyboard boolean
/// It also has a darkmode for the users with a good taste in styling.
class _EmojiBoard extends State<SimpleEmojiBar> {
  late TextSelection rememberPosition;

  double emojiKeyboardHeight = 100;

  TextEditingController? bromotionController;

  bool darkMode = false;

  @override
  void initState() {
    this.bromotionController = widget.emotionController;
    this.emojiKeyboardHeight = widget.emojiKeyboardHeight;
    this.darkMode = widget.darkMode;

    super.initState();
  }

  @override
  void didUpdateWidget(covariant SimpleEmojiBar oldWidget) {
    this.darkMode = widget.darkMode;
    this.emojiKeyboardHeight = widget.emojiKeyboardHeight;
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    super.dispose();
  }

  /// If the user selects an emoji from the grid a trigger is send to this
  /// function with the corresponding emoji that the user pressed.
  /// The emoji is added to the Textfield at the location of the cursor
  /// or as a replacement of the selection of the user.
  void insertText(String myText) {
    final text = bromotionController!.text;
    final textSelection = bromotionController!.selection;
    if (textSelection.isValid) {
      final newText = text.replaceRange(
        textSelection.start,
        textSelection.end,
        myText,
      );
      final myTextLength = myText.length;
      bromotionController!.text = newText;
      bromotionController!.selection = textSelection.copyWith(
        baseOffset: textSelection.start + myTextLength,
        extentOffset: textSelection.start + myTextLength,
      );
    } else {
      final myTextLength = myText.length;
      bromotionController!.text = myText;
      bromotionController!.selection = textSelection.copyWith(
        baseOffset: myTextLength,
        extentOffset: myTextLength,
      );
    }
  }

  bool isPortrait() {
    return MediaQuery.of(context).orientation == Orientation.portrait;
  }

  List<String> emojis = [
    "👍",
    "👎",
    "😭",
    "😕",
    "😐",
    "😊",
    "😍",
    "❤️",
    "🙌",
    "👏"
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.showEmojiKeyboard ? emojiKeyboardHeight : 0,
      color: this.darkMode ? Color(0xff373737) : Color(0xffc5c5c5),
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return TextButton(
              onPressed: () {
                insertText(emojis[index]);
              },
              child: FittedBox(
                fit: BoxFit.fitWidth,
                child: Text(emojis[index], style: TextStyle(fontSize: 50)),
              ));
        },
        itemCount: emojis.length,
      ),
    );
  }
}
