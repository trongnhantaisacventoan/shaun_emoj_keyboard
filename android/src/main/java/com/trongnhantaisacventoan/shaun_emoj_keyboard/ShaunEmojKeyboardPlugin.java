package com.trongnhantaisacventoan.shaun_emoj_keyboard;

import android.annotation.SuppressLint;
import android.graphics.Paint;
import android.os.Build;

import androidx.annotation.NonNull;

import java.util.List;
import java.util.Vector;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/** ShaunEmojKeyboardPlugin */
public class ShaunEmojKeyboardPlugin implements FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private MethodChannel channel;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "com.trongnhantaisacventoan.shaun_emoj_keyboard");
    channel.setMethodCallHandler(this);
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    if (call.method.equals("isAvailable")) {
      Paint paint = new Paint();
      List<String> emojisAvailable = call.argument("emojis");
      List<String> available = new Vector<>();
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (emojisAvailable != null) {
          for (String item : emojisAvailable) {
            if (paint.hasGlyph(item)) {
              available.add(item);
            }
          }
        }
      }
      result.success(available);
    } else {
      result.notImplemented();
    }
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
  }
}
