import Flutter
import UIKit

public class SwiftShaunEmojKeyboardPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "com.trongnhantaisacventoan.shaun_emoj_keyboard", binaryMessenger: registrar.messenger())
    let instance = SwiftShaunEmojKeyboardPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    result("iOS " + UIDevice.current.systemVersion)
  }
}
