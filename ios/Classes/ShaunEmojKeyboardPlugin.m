#import "ShaunEmojKeyboardPlugin.h"
#if __has_include(<shaun_emoj_keyboard/shaun_emoj_keyboard-Swift.h>)
#import <shaun_emoj_keyboard/shaun_emoj_keyboard-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "shaun_emoj_keyboard-Swift.h"
#endif

@implementation ShaunEmojKeyboardPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftShaunEmojKeyboardPlugin registerWithRegistrar:registrar];
}
@end
